from unicodedata import name
from django.urls import path
from receipts.views import (
    AccountCreateView,
    AccountListView,
    ExpenseCategoryCreate,
    ExpenseCategoryListView,
    ReceiptCreateView,
    ReceiptListView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path(
        "categories/create/",
        ExpenseCategoryCreate.as_view(),
        name="expense_category_create",
    ),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="account_create"
    ),
    path(
        "categories/", ExpenseCategoryListView.as_view(), name="expense_list"
    ),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
]
